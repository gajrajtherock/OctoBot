
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.


"""
Settings file for OctoBot-rewrite
"""
# Uncomment this in copied settings so you wont experience crashes when Updating bot
# from settings_example import *
# Logging level
# 10 - Debug
# 20 - Info
# 30 - Warning
# 40 - Error
# The lower, the more output
LOG_LEVEL = 20
# Your user ID
ADMIN = 174781687
# Bot token of MAIN bot from botfather
TOKEN = "PUTYOURTOKENHERE"
# Mirror tokens
# Example
# MIRRORS = {
#   "Mirror A":"Mirror A token",
#   "Mirror B":"Mirror B token"
# }
MIRRORS = {

}
# OctoBot needs this channel to make inline images work
# Put channel ID here
CHANNEL = 0
# This is required for "Reddit" plugin
# 'script' level required
REDDITID = ""
REDDITSECRET = ""
REDDITUA = ""
# This is Webhook Settings
# You should use webhook because it is
# better than polling telegram
WEBHOOK_ON = False
# Your server domain
WEBHOOK_URL = "https://example.com"
# How expose port:
# 127.0.0.1 - accept only localhost connections
# 0.0.0.0 - accept every connection.
# Default: 127.0.0.1
WEBHOOK_PORT_EXPOSE = "127.0.0.1"
WEBHOOK_PORT = 8443
# Yandex Translation API
# You can get your token from
# https://tech.yandex.com/translate/
YANDEX_TRANSLATION_TOKEN = ""
# Dev chat link
CHAT_LINK = "http://example.com"
# News channel link
NEWS_LINK = "http://example.com"
ABOUT_TEXT = """
Powered by Python-Telegram-Bot, Admin:@username
"""
# Thread count
THREADS = 32
# Fono Api token. Get yours from https://fonoapi.freshpixl.com/token/generate
FONOAPI_TOKEN = None
# Plugins to skip during loading process
SKIP_PLUGINS = []
# Extra links in /start.
# Example
# EXTRA_LINKS = [{"Test":"example.com"}]
EXTRA_LINKS = []
# sentry.io settings
USE_SENTRY = False
SENTRY_URL = ""
SENTRY_ENV = "production"
SENTRY_REPO = "OctoBot"

# Giphy token
GIPHY_TOKEN = ""

# Kwargs to pass to Bot class
BOT_KWARGS = {}

# Imgur Client ID
IMGUR_CID = ""

# AI name(s)
AI_NAMES = ["octobot"]

BACKPACK_TF_TOKEN = ""

# Bot website. /help will point to it
WEBSITE = ""
# This is DB settings for admin module
MONGO_DB_HOST = "127.0.0.1"
MONGO_DB_PORT = 27017
MONGO_DB_NAME = "admin_plug"
# Channel where all automatic ban notices will be logged
BANLOG_CHANNEL = None
# Key for /apex_PLATFORM commands, get one at http://api.mozambiquehe.re/getkey
MOZAMBIQUE_HERE_API_KEY = ""
