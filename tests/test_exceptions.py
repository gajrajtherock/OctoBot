try:
    import core
except ModuleNotFoundError:
    import os
    import sys
    import inspect
    currentdir = os.path.dirname(os.path.abspath(
        inspect.getfile(inspect.currentframe())))
    parentdir = os.path.dirname(currentdir)
    sys.path.insert(0, parentdir)
    import core
import os.path
import telegram
from hypothesis import given
from hypothesis.strategies import text
from datetime import datetime
import logging
USER = telegram.User(0, "Test User", is_bot=False, username="testuser")
CHAT = telegram.Chat(0, "private")


def test_exception_handling():
    ml = core.core.OctoBotCore(load_all=False)
    ml.load_plugin(os.path.normpath("tests/exception_plugin.py"))
    update = telegram.Update(1,
                             message=telegram.Message('0',
                                                      from_user=USER,
                                                      chat=CHAT,
                                                      date=datetime.now(),
                                                      text="/raise_exception"))
    ml.handle_command(update)
