import core
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()


@plugin.command(command="/raise_exception",
                description="Performs 1/0, resulting in exception",
                inline_supported=True,
                hidden=False)
def exception(bot, update, user, args):
    1 / 0
