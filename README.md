#yurikoBot


## A modular telegram bot

[![Telegram Stable Bot](https://img.shields.io/badge/Telegram-Bot-0088ccsvg)](http://t.me/yurikobot) [![Telegram Chat](https://img.shields.io/badge/Telegram-Chat-0088cc.svg)](https://t.me/aigis_dev) [![Telegram Channel](https://img.shields.io/badge/Telegram-Channel-0088cc.svg)](http://t.me/aigis_bot_channel)

### Installation

0..: `git submodule update --init --recursive`

1. `cp settings_example.py settings.py`

2. Extend the settings.py 

3. `pip3 install -r requirements.txt` - Install dependencies

Module telegram bot based on `Octo`V2 
Powered by `python-telegram-bot` wapper .

#Support for bot 
Try pinging me at tg username `@KingOfElephants`.where I can answer all your questions if you have ...
